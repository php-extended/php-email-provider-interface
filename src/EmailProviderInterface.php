<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\EmailProvider;

use Iterator;
use PhpExtended\Email\EmailInterface;
use PhpExtended\Email\EmailMetadataInterface;
use RuntimeException;
use Stringable;

/**
 * EmailProviderInterface interface file.
 * 
 * This class consists of a middleware that acts as gateway between the email
 * storage and programs that will use it. It provides two functions, one is
 * for listing the available emails with pagination, and the second is to get
 * the contents of a specific email.
 * 
 * @author Anastaszor
 */
interface EmailProviderInterface extends Stringable
{
	
	/**
	 * Gets the list of emails that are at the given page.
	 * 
	 * @param int $page
	 * @return Iterator<EmailMetadataInterface>
	 * @throws RuntimeException if the list cannot be retrieved
	 */
	public function listEmails(int $page = 0) : Iterator;
	
	/**
	 * Gets the email corresponding to the given metadata.
	 * 
	 * @param EmailMetadataInterface $metadata
	 * @return EmailInterface
	 * @throws RuntimeException if the email cannot be retrieved
	 */
	public function getEmail(EmailMetadataInterface $metadata) : EmailInterface;
	
}
